<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>LanItem</name>
    <message>
        <location filename="../lanitem.cpp" line="76"/>
        <source>Disconnect</source>
        <translation>断开连接</translation>
    </message>
    <message>
        <location filename="../lanitem.cpp" line="78"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
</context>
<context>
    <name>NetConnect</name>
    <message>
        <location filename="../netconnect.cpp" line="136"/>
        <source>LAN</source>
        <translation>有线网络</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="183"/>
        <source>Settings</source>
        <translation>网络设置</translation>
    </message>
</context>
</TS>
