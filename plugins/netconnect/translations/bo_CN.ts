<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>LanItem</name>
    <message>
        <location filename="../lanitem.cpp" line="76"/>
        <source>Disconnect</source>
        <translation>འབྲེལ་ཐག་ཆད་པ།</translation>
    </message>
    <message>
        <location filename="../lanitem.cpp" line="78"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
</context>
<context>
    <name>NetConnect</name>
    <message>
        <location filename="../netconnect.cpp" line="136"/>
        <source>LAN</source>
        <translation>སྐུད་ཡོད་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="183"/>
        <source>Settings</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
</context>
</TS>
