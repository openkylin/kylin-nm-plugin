
#LIBINTERFACE_NAME = $$qtLibraryTarget(divider)

SOURCES += \
        $$PWD/enterprisewlanpage.cpp \
        $$PWD/entsecuritywidget.cpp \
        $$PWD/hiddenwifipage.cpp \

HEADERS += \
        $$PWD/enterprisewlanpage.h \
        $$PWD/entsecuritywidget.h \
        $$PWD/hiddenwifipage.h \
