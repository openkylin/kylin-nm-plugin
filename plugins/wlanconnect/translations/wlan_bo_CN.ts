<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AddNetItem</name>
    <message>
        <location filename="../itemframe.cpp" line="15"/>
        <source>Add Others...</source>
        <translation>གཞན་པ་ཁ་སྣོན་བྱས་ནས་...</translation>
    </message>
</context>
<context>
    <name>EntSecurityWidget</name>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="125"/>
        <source>EAP type</source>
        <translation>EAP རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="127"/>
        <source>Identity</source>
        <translation>ཐོབ་ཐང་།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="128"/>
        <source>Domain</source>
        <translation>ཁྱབ་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="129"/>
        <source>CA certficate</source>
        <translation>CA certficate</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="130"/>
        <source>no need for CA certificate</source>
        <translation>CAཡི་ལག་ཁྱེར་མི་དགོས།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="131"/>
        <source>User certificate</source>
        <translation>སྤྱོད་མཁན་གྱི་ལག་ཁྱེར།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="132"/>
        <source>User private key</source>
        <translation>སྤྱོད་མཁན་གྱི་སྒེར་གྱི་ལྡེ་མིག</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="133"/>
        <source>User key password</source>
        <translation>སྤྱོད་མཁན་གྱི་ལྡེ་མིག་གི་གསང་</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="134"/>
        <source>Password options</source>
        <translation>གསང་བའི་ཐོག་ནས་རྣམ་གྲངས་བདམས་པ།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="136"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="137"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="145"/>
        <source>Required</source>
        <translation>ངེས་པར་དུ་སྐོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="141"/>
        <source>Ineer authentication</source>
        <translation>དབྱིན་ཆས་ཀྱི་བདེན་དཔང་ར་སྤྲོད་</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="142"/>
        <source>Usename</source>
        <translation>བཀོལ་སྤྱོད་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="143"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="154"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="160"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="166"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="326"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="352"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="380"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="409"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="156"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="162"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="168"/>
        <source>Choose from file...</source>
        <translation>ཡིག་ཆའི་ནང་ནས་གདམ་ག་རྒྱག་དགོས།...</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="158"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="164"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="170"/>
        <source>Please log in to the system first.</source>
        <translation>ཕུའུ་ལི་སི་ལོ་ཀེ་ནི་ཐུའོ་ཐི་ཞི་སི་ཐེ་མུའུ་ཧྥུ་སི་ཐེ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="172"/>
        <source>Store passwords only for this user</source>
        <translation>སྤྱོད་མཁན་དེ་ཁོ་ནའི་ཆེད་དུ་གསང་གྲངས་ཉར་ཚགས་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="173"/>
        <source>Store passwords for all users</source>
        <translation>སྤྱོད་མཁན་ཚང་མའི་གསང་བ་གསོག་ཉར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="174"/>
        <source>Ask this password every time</source>
        <translation>ཐེངས་རེར་གསང་བ་འདི་འདྲི་རྩད་བྱེད་ཐེངས་རེ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="342"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="371"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="399"/>
        <source>Choose a CA certificate</source>
        <translation>CAཡི་དཔང་ཡིག་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="343"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="372"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="400"/>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation>CA དཔང་ཡིག (*.pem *.der *.p12 *.crt *.cer *.pfx)</translation>
    </message>
</context>
<context>
    <name>EnterpriseWlanPage</name>
    <message>
        <location filename="../hiddenwifi/enterprisewlanpage.cpp" line="78"/>
        <source>Network name(SSID)</source>
        <translation>དྲ་རྒྱའི་མིང་། (SSID)</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/enterprisewlanpage.cpp" line="80"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/enterprisewlanpage.cpp" line="81"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/enterprisewlanpage.cpp" line="83"/>
        <source>Connect Enterprise WLAN</source>
        <translation>ཁེ་ལས་WLANས སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>HiddenWiFiPage</name>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="162"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="163"/>
        <source>WPA&amp;WPA2 Personal</source>
        <translation>WPA&amp;WPA2མི་སྒེར་གྱི་ངོས་ནས་བཤད་ན།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="164"/>
        <source>WPA&amp;WPA2 Enterprise</source>
        <translation>WPA&amp;WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="165"/>
        <source>WPA3 Personal</source>
        <translation>WPA3མི་སྒེར་</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="168"/>
        <source>Please enter the network name and security type</source>
        <translation>ཁྱེད་རང་དྲ་རྒྱའི་ནང་དུ་ཞུགས་འདོད་པའི་མིང་དང་བདེ་འཇགས་རིགས་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="172"/>
        <source>Network name(SSID)</source>
        <translation>དྲ་རྒྱའི་མིང་། (SSID)</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="173"/>
        <source>Security type</source>
        <translation>སའེ་ཁི་ལི་ཏི་ཐའེ་ཕེ།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="174"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="175"/>
        <source>Remember the Network</source>
        <translation>དྲ་རྒྱ་དེ་སེམས་ལ་འཛིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="177"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="178"/>
        <source>Join</source>
        <translation>དེའི་ནང་དུ་ཞུགས་པ།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="181"/>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="182"/>
        <source>Required</source>
        <translation>ངེས་པར་དུ་སྐོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="190"/>
        <source>Find and Join Wi-Fi</source>
        <translation>འཚོལ་ཞིབ་བྱས་པ་མ་ཟད་WI-FIལ་ཞུགས་པ་རེད།</translation>
    </message>
</context>
<context>
    <name>LogHintDialog</name>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="543"/>
        <source>Please log in to the system first.</source>
        <translation>ཕུའུ་ལི་སི་ལོ་ཀེ་ནི་ཐུའོ་ཐི་ཞི་སི་ཐེ་མུའུ་ཧྥུ་སི་ཐེ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="544"/>
        <source>Confirm</source>
        <translation>ཁང་ཧྥེར་མུའུ།</translation>
    </message>
</context>
<context>
    <name>WlanConnect</name>
    <message>
        <location filename="../wlanconnect.cpp" line="186"/>
        <source>WLAN</source>
        <translation>སྐུད་མེད་ཅུས་ཁོངས་ཀྱི་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="234"/>
        <source>Settings</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
</context>
<context>
    <name>WlanItem</name>
    <message>
        <location filename="../wlanitem.cpp" line="75"/>
        <location filename="../wlanitem.cpp" line="173"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../wlanitem.cpp" line="95"/>
        <source>Auto Connect</source>
        <translation>རང་འགུལ་གྱིས་སྦྲེལ་མཐུད་</translation>
    </message>
    <message>
        <location filename="../wlanitem.cpp" line="171"/>
        <source>Disconnect</source>
        <translation>འབྲེལ་ཐག་ཆད་པ།</translation>
    </message>
</context>
</TS>
