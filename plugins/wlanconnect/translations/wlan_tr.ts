<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AddNetItem</name>
    <message>
        <location filename="../itemframe.cpp" line="15"/>
        <source>Add Others...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EntSecurityWidget</name>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="125"/>
        <source>EAP type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="127"/>
        <source>Identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="128"/>
        <source>Domain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="129"/>
        <source>CA certficate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="130"/>
        <source>no need for CA certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="131"/>
        <source>User certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="132"/>
        <source>User private key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="133"/>
        <source>User key password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="134"/>
        <source>Password options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="136"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="137"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="145"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="141"/>
        <source>Ineer authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="142"/>
        <source>Usename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="143"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="154"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="160"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="166"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="326"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="352"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="380"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="409"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="156"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="162"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="168"/>
        <source>Choose from file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="158"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="164"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="170"/>
        <source>Please log in to the system first.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="172"/>
        <source>Store passwords only for this user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="173"/>
        <source>Store passwords for all users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="174"/>
        <source>Ask this password every time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="342"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="371"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="399"/>
        <source>Choose a CA certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="343"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="372"/>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="400"/>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EnterpriseWlanPage</name>
    <message>
        <location filename="../hiddenwifi/enterprisewlanpage.cpp" line="78"/>
        <source>Network name(SSID)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/enterprisewlanpage.cpp" line="80"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/enterprisewlanpage.cpp" line="81"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/enterprisewlanpage.cpp" line="83"/>
        <source>Connect Enterprise WLAN</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HiddenWiFiPage</name>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="162"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="163"/>
        <source>WPA&amp;WPA2 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="164"/>
        <source>WPA&amp;WPA2 Enterprise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="165"/>
        <source>WPA3 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="168"/>
        <source>Please enter the network name and security type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="172"/>
        <source>Network name(SSID)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="173"/>
        <source>Security type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="174"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="175"/>
        <source>Remember the Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="177"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="178"/>
        <source>Join</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="181"/>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="182"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/hiddenwifipage.cpp" line="190"/>
        <source>Find and Join Wi-Fi</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LogHintDialog</name>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="543"/>
        <source>Please log in to the system first.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hiddenwifi/entsecuritywidget.cpp" line="544"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WlanConnect</name>
    <message>
        <location filename="../wlanconnect.cpp" line="186"/>
        <source>WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="234"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WlanItem</name>
    <message>
        <location filename="../wlanitem.cpp" line="75"/>
        <location filename="../wlanitem.cpp" line="173"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wlanitem.cpp" line="95"/>
        <source>Auto Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wlanitem.cpp" line="171"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
